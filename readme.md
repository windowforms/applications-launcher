# EnablePrograms Launcher

A convenient launcher for developers, designed to start essential tools like Unity, JetBrains Rider, Fork, Chrome with predefined URLs, and Slack from a single interface.

## Features

- Launches Unity and Rider projects.
- Opens Fork for repository management.
- Automates Chrome to open multiple URLs in a specified profile.
- Starts Slack for communication.

## Quick Start

1. **Prerequisites**: Ensure .NET Framework is installed.
2. **Installation**: Download and run `EnablePrograms.exe`.
3. **Usage**: Select the applications to launch and click 'Approve'.

## How It Works

Select your preferred development tools from the checklist and approve to launch them together, streamlining your workflow.

## Contributing

Feedback and contributions are welcome. Please submit pull requests or issues through our GitHub repository.

