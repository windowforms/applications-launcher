﻿using System.Diagnostics;

namespace EnablePrograms.LaunchApplications.ApplicationEntities
{
    public class ChromeApplication : IApplicationEntity
    {
        private const string ChromePath = @"C:\Program Files\Google\Chrome\Application\chrome.exe";
        private const string ProfileDirPath = @"C:\Users\mirsk\AppData\Local\Google\Chrome\User Data";
        
        private readonly string profileName;
        private readonly string urls;

        public ChromeApplication(string profileName, string urls)
        {
            this.profileName = profileName;
            this.urls = urls;
        }

        public void Launch()
        {
            Process.Start(ChromePath, $"--user-data-dir=\"{ProfileDirPath}\" --profile-directory=\"{profileName}\" {urls}");
        }

        public bool IsChecked { get; set; } = true;
    }
}