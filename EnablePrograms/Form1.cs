﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EnablePrograms.LaunchApplications.ApplicationEntities;

namespace EnablePrograms
{
    public partial class Form1 : Form
    {
        private Dictionary<string, IApplicationEntity> _applications;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _applications = new Dictionary<string, IApplicationEntity>
            {
                {
                    ProgramName.Unity.ToString(),
                    new UnityApplication(unityProjectPath: @"E:\Work\GamesGalaxy\client")
                },
                {
                    ProgramName.Rider.ToString(),
                    new RiderApplication(applicationPath: @"E:\Work\GamesGalaxy\client\client.sln")
                },
                { ProgramName.Fork.ToString(), new ForkApplication() },
                {
                    ProgramName.Chrome.ToString(), new ChromeApplication("Profile 4",
                        "https://galaxy4games.atlassian.net/jira/software/projects/M3S/boards/3?assignee=60ec0c3ccc1f700071f05476 " +
                        "http://m3-dev.match-stars.com/admin/events " +
                        "http://m3-dev.match-stars.com/admin/users/204166 " +
                        "https://gitlab.com/matchstars/client/-/merge_requests")
                },
                { ProgramName.Slack.ToString(), new SlackApplication() }
            };

            checkedListPrograms.Items.Clear();

            foreach (string applicationName in _applications.Keys)
                checkedListPrograms.Items.Add(applicationName);

            for (int i = 0; i < checkedListPrograms.Items.Count; i++)
                checkedListPrograms.SetItemChecked(i, _applications[checkedListPrograms.Items[i].ToString()].IsChecked);
        }

        private static void LaunchApplication(string name, IApplicationEntity application)
        {
            try
            {
                application.Launch();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error launching {name}: {ex.Message}", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void approveButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListPrograms.Items.Count; i++)
            {
                var name = checkedListPrograms.Items[i].ToString();

                if (checkedListPrograms.GetItemChecked(i))
                    LaunchApplication(name, _applications[name]);
            }
        }

        public enum ProgramName
        {
            Unity,
            Rider,
            Fork,
            Chrome,
            Slack
        }

        private void checkedListPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            var index = checkedListPrograms.SelectedIndex;
            if (index < 0) return;

            var name = checkedListPrograms.Items[index].ToString();
            _applications[name].IsChecked = checkedListPrograms.GetItemChecked(index);
        }
    }
}