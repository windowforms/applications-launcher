﻿using System.Diagnostics;

namespace EnablePrograms.LaunchApplications.ApplicationEntities
{
    public class ForkApplication : IApplicationEntity
    {
        public void Launch()
        {
            Process.Start(@"C:\Users\mirsk\AppData\Local\Fork\Fork.exe");
        }

        public bool IsChecked { get; set; } = true;
    }
}