﻿
namespace EnablePrograms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListPrograms = new System.Windows.Forms.CheckedListBox();
            this.approveButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // checkedListPrograms
            // 
            this.checkedListPrograms.FormattingEnabled = true;
            this.checkedListPrograms.Location = new System.Drawing.Point(38, 37);
            this.checkedListPrograms.Name = "checkedListPrograms";
            this.checkedListPrograms.Size = new System.Drawing.Size(161, 157);
            this.checkedListPrograms.TabIndex = 1;
            this.checkedListPrograms.SelectedIndexChanged += new System.EventHandler(this.checkedListPrograms_SelectedIndexChanged);
            // 
            // approveButton
            // 
            this.approveButton.Location = new System.Drawing.Point(44, 200);
            this.approveButton.Name = "approveButton";
            this.approveButton.Size = new System.Drawing.Size(136, 23);
            this.approveButton.TabIndex = 2;
            this.approveButton.Text = "Launch";
            this.approveButton.UseVisualStyleBackColor = true;
            this.approveButton.Click += new System.EventHandler(this.approveButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(33, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Launch Programs";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.approveButton);
            this.Controls.Add(this.checkedListPrograms);
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.CheckedListBox checkedListPrograms;
        private System.Windows.Forms.Button approveButton;
        private System.Windows.Forms.Label label1;

        #endregion
    }
}

