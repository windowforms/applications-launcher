﻿using System.Diagnostics;

namespace EnablePrograms.LaunchApplications.ApplicationEntities
{
    public class UnityApplication : IApplicationEntity
    {
        private readonly string unityProjectPath;

        public UnityApplication(string unityProjectPath)
        {
            this.unityProjectPath = unityProjectPath;
        }
        
        public void Launch()
        {
            Process.Start(@"D:\Program Files\Unity\Hub\Editor\2022.3.16f1\Editor\Unity.exe", $"-projectPath \"{unityProjectPath}\"");
        }

        public bool IsChecked { get; set; } = true;
    }
}