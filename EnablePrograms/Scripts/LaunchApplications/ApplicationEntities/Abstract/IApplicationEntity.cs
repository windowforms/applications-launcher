﻿namespace EnablePrograms.LaunchApplications.ApplicationEntities
{
    public interface IApplicationEntity
    {
        void Launch();
        bool IsChecked { get; set; }
    }
}