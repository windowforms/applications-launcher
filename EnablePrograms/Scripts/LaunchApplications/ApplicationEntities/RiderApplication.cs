﻿using System.Diagnostics;

namespace EnablePrograms.LaunchApplications.ApplicationEntities
{
    public class RiderApplication : IApplicationEntity
    {
        private readonly string _applicationPath;

        public RiderApplication(string applicationPath)
        {
            _applicationPath = applicationPath;
        }

        public void Launch()
        {
            Process.Start(@"D:\Program Files\JetBrains\JetBrains Rider 2022.3.1\bin\rider64.exe", _applicationPath);
        }
        
        public bool IsChecked { get; set; } = true;
    }
}