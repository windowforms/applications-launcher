﻿using System.Diagnostics;

namespace EnablePrograms.LaunchApplications.ApplicationEntities
{
    public class SlackApplication : IApplicationEntity
    {
        private const string SlackPath = @"C:\Users\mirsk\AppData\Local\slack\slack.exe";

        public void Launch()
        {
            Process.Start(SlackPath);
        }

        public bool IsChecked { get; set; } = true;
    }
}